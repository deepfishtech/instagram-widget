<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
		<!-- Список в который будут выводится фото -->
	<ul id="list"></ul>





	<script src="jquery.js"></script>

	<script>
		var resolution = "low";    // API предоставляет три разрешения фото (thumbnail, low, standard), по умолчанию подгружается standard
		var imgArr = [];		   // Массив с изображениями

		$.ajax({
			url: 'getRecentPhotos.php',
			dataType: 'json',
			type: 'POST',
			data: {count: 6},
			success: function(data){
				console.log(data);
		 		for(x in data.data){

		 			imgArr[x] = {url: '',link: ''}

		 			if(resolution == "thumbnail"){
		 				imgArr[x].url = data.data[x].images.thumbnail.url;
		 			} else if(resolution == "low") {
	 					imgArr[x].url = data.data[x].images.low_resolution.url;
		 			} else if(resolution == "standard"){
		 				imgArr[x].url = data.data[x].images.standard_resolution.url;
		 			} else {
		 				imgArr[x].url = data.data[x].images.standard_resolution.url;
		 			}

		 			imgArr[x].link = data.data[x].link;

		 		}

		 		console.log(imgArr);

		 		// Действия выполняемые после загрузки
		 		$.each(imgArr, function(i, img){
					$("#list").append('<li class="col-4 bandsclub_list_item"><a target="_blank" href="'+img.link+'"><img class="bandsclub_list_img" src="'+img.url+'" alt=""></li></a>');
				});

			},
			error: function(data){
				console.log(data);
			}
		});
		

	</script>
</body>
</html>